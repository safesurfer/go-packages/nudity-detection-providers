package nuditydetectionproviders

import (
	"context"
	"fmt"
	"io"

	vision "cloud.google.com/go/vision/apiv1"
	"google.golang.org/api/option"
)

func newGoogleCloudVisionDriver(ctx context.Context, logger LoggingFunc, config *GoogleCloudVisionConfig) (driver *googleCloudVisionDriver, err error) {
	driver = &googleCloudVisionDriver{config: config, logger: logger}
	driver.client, err = vision.NewImageAnnotatorClient(ctx, option.WithCredentialsJSON([]byte(config.Credentials)))
	if err != nil {
		return nil, err
	}
	return driver, nil
}

func (d *googleCloudVisionDriver) Get(ctx context.Context, reader io.Reader) (detected bool, err error) {
	image, err := vision.NewImageFromReader(reader)
	if err != nil {
		return false, fmt.Errorf("couldn't create image from reader: %w", err)
	}
	props, err := d.client.DetectSafeSearch(ctx, image, nil)
	if err != nil {
		return false, fmt.Errorf("couldn't detect image labels: %w", err)
	}
	if int(props.GetAdult()) >= d.config.RequireAdultLevel {
		return true, nil
	}
	if int(props.GetRacy()) >= d.config.RequireRacyLevel {
		return true, nil
	}
	return false, nil
}

func (d *googleCloudVisionDriver) Close() error {
	return d.client.Close()
}
