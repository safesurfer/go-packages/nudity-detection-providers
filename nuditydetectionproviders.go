package nuditydetectionproviders

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v3"
)

type ErrBadConfig struct {
	Err error
}

func (b *ErrBadConfig) Error() string {
	return fmt.Sprintf("nudity detection providers: bad config: %v", b.Err.Error())
}

func New(ctx context.Context, config *NudityDetectionConfig, logger LoggingFunc) (client NudityDetectionDriver, err error) {
	instance := &nudityDetectionInstance{}
	if config.GoogleCloudVision != nil {
		instance.driver, err = newGoogleCloudVisionDriver(ctx, logger, config.GoogleCloudVision)
		if err != nil {
			return nil, err
		}
	} else {
		return nil, &ErrBadConfig{errors.New("no provider defined")}
	}
	return instance, nil
}

func NewFromEnv(ctx context.Context, logger LoggingFunc) (client NudityDetectionDriver, err error) {
	config := &NudityDetectionConfig{}
	driver := os.Getenv("NUDITY_DETECTION_PROVIDER")
	switch driver {
	case "googleCloudVision":
		config.GoogleCloudVision = &GoogleCloudVisionConfig{
			Credentials: os.Getenv("NUDITY_DETECTION_GOOGLE_CLOUD_VISION_CREDENTIALS"),
		}
	default:
		return nil, &ErrBadConfig{fmt.Errorf("bad value for env NUDITY_DETECTION_PROVIDER %v: valid choices are googleCloudVision", driver)}
	}
	return New(ctx, config, logger)
}

func NewFromConfigFile(ctx context.Context, path string, logger LoggingFunc) (NudityDetectionDriver, error) {
	configFile, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, &ErrBadConfig{fmt.Errorf("file %v couldn't be read: %v", path, err)}
	}
	if logger != nil {
		logger("found config file: %v", string(configFile))
	}
	var config NudityDetectionConfig
	err = yaml.Unmarshal(configFile, &config)
	if err != nil {
		return nil, &ErrBadConfig{fmt.Errorf("file %v couldn't be converted from yaml: %v", path, err)}
	}
	return New(ctx, &config, logger)
}

func (i *nudityDetectionInstance) Get(ctx context.Context, reader io.Reader) (detected bool, err error) {
	return i.driver.Get(ctx, reader)
}

func (i *nudityDetectionInstance) Close() error {
	return i.driver.Close()
}
