package nuditydetectionproviders

import (
	"context"
	"io"

	vision "cloud.google.com/go/vision/apiv1"
)

type LoggingFunc func(string, ...interface{})

type NudityDetectionConfig struct {
	GoogleCloudVision *GoogleCloudVisionConfig `yaml:"googleCloudVision"`
}

type GoogleCloudVisionConfig struct {
	// The gcp service account JSON file that will allow
	// access to Cloud Vision when used as auth.
	Credentials string `yaml:"credentials"`
	// The required level of the "racy" category before detecting.
	// 1 is very unlikely, 5 is very likely.
	RequireRacyLevel int `yaml:"requireRacyLevel"`
	// The required level of the "adult" category before detecting.
	// 1 is very unlikely, 5 is very likely.
	RequireAdultLevel int `yaml:"requireAdultLevel"`
}

type googleCloudVisionDriver struct {
	logger LoggingFunc
	config *GoogleCloudVisionConfig
	client *vision.ImageAnnotatorClient
}

type nudityDetectionInstance struct {
	driver NudityDetectionDriver
}

type NudityDetectionDriver interface {
	Get(ctx context.Context, reader io.Reader) (detected bool, err error)
	Close() error
}
